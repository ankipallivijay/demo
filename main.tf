module "server" {
  source           = "github.com/MohankrishnaSSI/terraform_module.git"
  region           = var.region
  ami              = var.ami
  key_name         = var.key_name
  instance_type    = var.instance_type
  private_key_path = var.private_key_path
}